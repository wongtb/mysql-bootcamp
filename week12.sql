-- Exercise 1:
/*
CREATE TABLE students(
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(100)
);

CREATE TABLE papers(
    title VARCHAR(100), 
    grade INT,
    student_id INT,
    FOREIGN KEY(student_id) REFERENCES students(id)
); 



-- Exercise 2: 

INSERT INTO students (first_name) VALUES
('Calebi'), ('Samantha'), ('Raj'), ('Carlos'), ('Lisa'); 

INSERT INTO papers (student_id, title, grade ) VALUES
(1, 'My First Book Report', 60),
(1, 'My Second Book Report', 75),
(2, 'Russian List Through The Ages', 94),
(2, 'De Montaigne and The At of The Essay', 98), 
(4, 'Borges and Magical Realism', 89); 

*/

-- Exercise 3:
-- Left is students, right is papers
/*
SELECT first_name, title, grade 
    FROM students
    RIGHT JOIN papers
        ON students.id = papers.student_id
        ORDER BY grade desc
*/

-- Exercise 4: 
/*
SELECT first_name, title, grade 
    FROM students
    LEFT JOIN papers
        ON students.id = papers.student_id
*/

-- Exercise 5:
/*
SELECT first_name, IFNULL(title, "MISSING") as 'title', IFNULL(grade, 0) as 'grade' 
    FROM students
    LEFT JOIN papers
        ON students.id = papers.student_id
*/

-- Exercise 6:
/*
SELECT first_name, ifnull(avg(grade), 0) as 'average', if(avg(grade) >= 75, 'PASSING', 'FAILING') as 'passing_status'
    FROM students
    LEFT JOIN papers
        ON students.id = papers.student_id
        GROUP BY first_name
        ORDER BY average desc