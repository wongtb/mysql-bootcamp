/* Week 8 Code */

/* Exercise 1 */
-- Select COUNT(title) from books; 

/* Exercise 2 */
-- Select released_year, COUNT(title) from books group by released_year; 

/* Exercise 3 */
-- Select sum(stock_quantity) from books;

/* Exercise 4 */
--  Select author_lname, author_fname, avg(released_year) from books group by author_lname, author_fname;

/* Exercise 5 */
-- Select concat(author_lname, ' ', author_fname) as 'full name' from books order by released_year desc limit 1;

/* Exercise 6 */
--  select min(released_year) as 'year', count(title) as '# books', avg(pages) as 'avg pages' from books group by author_lname, author_fname order by min(released_year) asc; 
