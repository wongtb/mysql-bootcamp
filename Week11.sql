-- Exercise 1:
-- a. SELECT 10 ! = 10 -> 0
-- b. SELECT 15 > 14 && 99 - 5 <= 94 -> 1
-- c. SELECT 1 IN (5,3) || 9 BETWEEN 8 AND 10; -> 1


-- Exercise 2: Select All Books Written Before 1980 (non inclusive)
select * from books where released_year < 1980;

-- Exercise 3: Select all books written by Eggers or ChabonALTER
select * from books where author_lname = 'Eggers' || author_lname = 'Chabon';

-- Exercise 4: Select all books written by Lahiri, published after 2000
select * from books where author_lname = 'Lahiri' && released_year > 2000; 

-- Exercise 5: Select all books between 100 and 200 pages
select * from books where pages between 100 and 200;

-- Exercise 6: Select all books where author name starts with a C or S
select * from books where author_lname like 'S%' || author_lname like 'C%'; 

-- Exercise 7: 
select title, author_lname,  
    CASE
        WHEN title LIKE '%stories%' THEN 'Short Stories'
        WHEN (title LIKE '%just kids%' || title LIKE '%a heartbreaking work%') THEN 'Memoir'
        ELSE 'Novel'
    END AS 'TYPE'
FROM books;

-- Exercise 8
SELECT author_lname, 
    CASE
        WHEN COUNT(*) = 1 THEN '1 book' 
        ELSE CONCAT(COUNT(*), ' books')
    END AS 'COUNT'
FROM books GROUP BY author_lname, author_fname; 
        