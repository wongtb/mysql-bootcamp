/* Week 7 Code */

/* Exercise 1 */
-- SELECT REVERSE(UPPER("Why does my cat look at me with such hatred?"));

/* Exercise 2 */
-- Prints "I like cats -"

/* Exercise 3 */
-- SELECT REPLACE(title, ' ', '->') as 'title' from books;

/* Exercise 4 */
-- SELECT author_lname as 'forwards', reverse(author_lname) as 'backwards' from books;

/* Exercise 5 */
-- SELECT CONCAT(title, ' was released in ', released_year) as 'blurb' from books;

/* Exercise 6 */
-- SELECT title, length(title) as 'character_count' from books;

/* Exercise 7 */
-- SELECT CONCAT(SUBSTRING(title,1, 10), '...') as 'short title', CONCAT(author_lname, ',', author_fname) as 'author', CONCAT(stock_quantity, ' in stock') as 'quantity' from books WHERE SUBSTRING(title, 1,1)='A'
